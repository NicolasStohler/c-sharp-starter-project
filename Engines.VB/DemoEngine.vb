﻿Imports AppConfiguration
Imports Data.Repository.Contracts.Repository_Interfaces
Imports Engines.Contracts
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports Core.Common.LibLog.Logging

Friend Class DemoEngine
	Implements IDemoEngine
	Private Shared ReadOnly _Logger As ILog = LogProvider.GetCurrentClassLogger()

	Private ReadOnly _AppConfig As IAppConfig
	Private ReadOnly _CustomerRepository As ICustomerRepository
	Private ReadOnly _OrderRepository As IOrderRepository

	Public Sub New(appConfig As IAppConfig, customerRepository As ICustomerRepository, orderRepository As IOrderRepository)
		_AppConfig = appConfig
		_CustomerRepository = customerRepository
		_OrderRepository = orderRepository
	End Sub

	Public Sub Executex() Implements IDemoEngine.Execute
		_Logger.Info("Execute start")

		_Logger.InfoFormat("from AppConfig: {setting}, {value}, {date}", _AppConfig.MySetting, _AppConfig.MyValue, _AppConfig.MyDate.ToShortDateString())

		Dim customerSet = _CustomerRepository.Get()

		For Each customer In customerSet
			Dim orderSet = _OrderRepository.GetByCustomerId(customer.CustomerId)
			_Logger.InfoFormat("CustomerId: {name} ({id}) - {company}, Order Count = {orderCount}", customer.ContactName, customer.CustomerId, customer.CompanyName, orderSet.Count())
			For Each order In orderSet
				_Logger.InfoFormat("  - order ({id}): {quantity}, {date}", order.OrderId, order.OrderQuantity, order.OrderDate?.ToShortDateString())
			Next
		Next

		_Logger.Info("Execute end")
	End Sub

End Class
