﻿

Imports Autofac

Public Class AutofacRegistrationModule
	Inherits [Module]

	Protected Overrides Sub Load(builder As ContainerBuilder)
		MyBase.Load(builder)

		' builder.RegisterType<AppConfig>().As<IAppConfig>().InstancePerLifetimeScope();

		builder.RegisterAssemblyTypes(ThisAssembly).AsImplementedInterfaces()

	End Sub
End Class

'=======================================================
'Service provided by Telerik (www.telerik.com)
'Conversion powered by NRefactory.
'Twitter: @telerik
'Facebook: facebook.com/telerik
'=======================================================
