﻿
Imports Autofac
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports Core.Common.LibLog.Logging


Public NotInheritable Class HostBooststrapper
	Private Sub New()
	End Sub
	Private Shared ReadOnly _Logger As ILog = LogProvider.GetCurrentClassLogger()

	Private Shared ReadOnly _HostBootstrapperService As New Lazy(Of HostBootstrapperService)(AddressOf Initialize)

	Private Shared Function Initialize() As HostBootstrapperService
		Dim hostBootstrapperService = New HostBootstrapperService()
		Return hostBootstrapperService
	End Function

	Public Shared ReadOnly Property AutofacContainer() As IContainer
		Get
			Return _HostBootstrapperService.Value.AutofacContainer
		End Get
	End Property
End Class

'=======================================================
'Service provided by Telerik (www.telerik.com)
'Conversion powered by NRefactory.
'Twitter: @telerik
'Facebook: facebook.com/telerik
'=======================================================
