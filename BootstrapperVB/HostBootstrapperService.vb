﻿
Imports Autofac
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports BootstrapperVB.Bootstrapper
Imports Core.Common.LibLog.Logging

Friend Class HostBootstrapperService
	Private Shared ReadOnly _Logger As ILog = LogProvider.GetCurrentClassLogger()
	Public ReadOnly Property AutofacContainer() As IContainer
		Get
			Return _AutofacContainer
		End Get
	End Property

	Private ReadOnly _AutofacContainer As IContainer

	Public Sub New()
		_Logger.Info("HostBootstrapperService.CTOR")

		' other initializations go here...

		_AutofacContainer = InitializeAutofacContainer(AddressOf BuilderFunction)
	End Sub

	Public Shared Sub BuilderFunction(builder As ContainerBuilder)

	End Sub

	Private Shared Function InitializeAutofacContainer(containerBuilderFn As Action(Of ContainerBuilder)) As IContainer
		Return AutofacLoader.Initialize(containerBuilderFn)
	End Function
End Class

'=======================================================
'Service provided by Telerik (www.telerik.com)
'Conversion powered by NRefactory.
'Twitter: @telerik
'Facebook: facebook.com/telerik
'=======================================================
