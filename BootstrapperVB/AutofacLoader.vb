﻿

Imports Autofac

Namespace Bootstrapper
	Public NotInheritable Class AutofacLoader
		Private Sub New()
		End Sub
		Public Shared Function Initialize(containerBuilderFn As Action(Of ContainerBuilder)) As IContainer
			Dim container As IContainer = Nothing
			Dim builder As New ContainerBuilder()

			' register types here

			' using Autofac-Modules:
			builder.RegisterAssemblyModules(GetType(Data.Repository.AutofacRegistrationModule).Assembly)
			builder.RegisterAssemblyModules(GetType(Engines.VB.AutofacRegistrationModule).Assembly)
			builder.RegisterAssemblyModules(GetType(AppConfiguration.AutofacRegistrationModule).Assembly)
			
			' invoke containerBuilderFn if provided
			containerBuilderFn?.Invoke(builder)

			container = builder.Build()

			Return container
		End Function
	End Class
End Namespace

'=======================================================
'Service provided by Telerik (www.telerik.com)
'Conversion powered by NRefactory.
'Twitter: @telerik
'Facebook: facebook.com/telerik
'=======================================================
