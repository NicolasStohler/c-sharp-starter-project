﻿
Imports Autofac
Imports Serilog
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Threading.Tasks
Imports Core.Common.LibLog.Logging
Imports EnsureThat
Imports Engines.Contracts
Imports BootstrapperVB

Namespace Executable.Console
	 Class Program
		Private Shared _Logger As ILog

		Public Shared Sub Main(args As String())
			InitializeLogger()

			SetupDataDirectory()

			Dim autofacContainer = BootstrapperVB.HostBooststrapper.AutofacContainer

			Try
				Using scope = autofacContainer.BeginLifetimeScope()

					Dim demoEngine = scope.Resolve(Of IDemoEngine)()
					Ensure.That(demoEngine, "demoEngine").IsNotNull()

					demoEngine.Execute()
				End Using
			Catch ex As Exception
				_Logger.ErrorException("Error", ex)
			End Try

		End Sub

		Private Shared Sub SetupDataDirectory()
			' set up DataDirectory in AppDomain for use in app.config connection string
			' app.config: "...AttachDbFilename=|DataDirectory|\DatabaseX.mdf..."
			Dim executable As String = System.Reflection.Assembly.GetExecutingAssembly().Location
			Dim path As String = (System.IO.Path.GetDirectoryName(executable))

			' navigate up 3 directories, point to Data dir where db resides
			path = System.IO.Path.GetFullPath(System.IO.Path.Combine(path, "..\..\..\Data"))

			AppDomain.CurrentDomain.SetData("DataDirectory", path)

			_Logger.InfoFormat("AppDomain.DataDirectory: {path}", path)
		End Sub

		Private Shared Sub InitializeLogger()
			Log.Logger = New LoggerConfiguration().Destructure.ByTransforming(Of System.IO.FileInfo)(Function(f) New With { _
				Key .Name = f.Name, _
				Key .Full = f.FullName, _
				Key .Exists = f.Exists, _
				Key .Attributes = f.Attributes _
			}).MinimumLevel.Verbose().WriteTo.LiterateConsole(restrictedToMinimumLevel := Serilog.Events.LogEventLevel.Debug).WriteTo.RollingFile("logs\myApp-{Date}.txt", restrictedToMinimumLevel := Serilog.Events.LogEventLevel.Verbose).CreateLogger()

			Log.Information("App.VB Startup (Serilog Log.Information)")

			_Logger = LogProvider.GetCurrentClassLogger()
			_Logger.Info("Startup.VB (via LibLog interface)")
		End Sub
	End Class
End Namespace
