﻿using System.Linq;
using Autofac;
using Data.Repository.Contracts;

namespace Data.Repository
{
	public class AutofacRegistrationModule : Module
	{
		protected override void Load(ContainerBuilder builder)
		{
			base.Load(builder);

			// this works too:
			//builder.RegisterAssemblyTypes(ThisAssembly)
			//	.AsImplementedInterfaces();

			// ...but as an explicit registration example:
			builder.RegisterAssemblyTypes(ThisAssembly)
				.Where(t => t.Name.EndsWith("Repository"))
				.As(t => t.GetInterfaces().FirstOrDefault(i => i.Name == "I" + t.Name));

			builder.RegisterType<DataRepositoryFactory>().As<IDataRepositoryFactory>();

		}
	}
}
