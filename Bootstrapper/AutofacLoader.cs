﻿using Autofac;
using System;

namespace Bootstrapper
{
	public static class AutofacLoader
	{
		public static IContainer Initialize(Action<ContainerBuilder> containerBuilderFn)
		{
			IContainer container = null;
			ContainerBuilder builder = new ContainerBuilder();

			// register types here

			// using Autofac-Modules:
			builder.RegisterAssemblyModules(typeof(Data.Repository.AutofacRegistrationModule).Assembly);
			builder.RegisterAssemblyModules(typeof(Engines.AutofacRegistrationModule).Assembly);
			builder.RegisterAssemblyModules(typeof(AppConfiguration.AutofacRegistrationModule).Assembly);

			// invoke containerBuilderFn if provided
			containerBuilderFn?.Invoke(builder);

			container = builder.Build();

			return container;
		}
	}
}
